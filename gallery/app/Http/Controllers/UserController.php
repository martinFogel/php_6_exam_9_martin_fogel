<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $photos = Photo::where('user_id', auth()->user()->id)->get();
        return view('client.users.index', compact('photos'));
    }

    /**
     * @param User $user
     * @return Application|Factory|View
     */
    public function show(User $user)
    {
        $photos = Photo::where('user_id', auth()->user()->id)->get();
        return view('client.users.index', compact('photos'));
    }
}
