<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Photo;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * @param Request $request
     * @param Photo $photo
     * @return RedirectResponse
     */
    public function store(Request $request, Photo $photo)
    {
        $request->validate([
            'body' => 'required|min:5',
            'rating' => 'required|in:1,2,3,4,5'
        ]);
        $comment = new Comment();
        $comment->user_id = $request->user()->id;
        $comment->body = $request->input('body');
        $comment->rating = $request->input('rating');
        $comment->photo_id = $photo->id;
        $comment->save();
        return back();
    }

    /**
     * @param Comment $comment
     * @return View|Factory|Application
     */
    public function edit(Comment $comment)
    {
        $this->authorize('update', $comment);
        return view('client.comments.edit', compact('comment'));
    }

    /**
     * @param Request $request
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function update(Request $request, Comment $comment)
    {
        $this->authorize('update', $comment);
        $request->validate([
            'body' => 'required|min:5',
            'rating' => 'required|in:1,2,3,4,5'
        ]);
        $comment->update($request->all());
        return redirect()->route('photos.show', $comment->photo_id);
    }

    /**
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function destroy(Comment $comment)
    {
        $this->authorize('delete', $comment);
        $comment->delete();
        return redirect()->route('photos.show', $comment->photo_id);
    }
}
