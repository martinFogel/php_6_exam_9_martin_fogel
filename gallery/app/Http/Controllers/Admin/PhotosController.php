<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Form\PhotoForm;
use App\Http\Requests\GenreRequest;
use App\Http\Requests\PhotoRequest;
use App\Models\Genre;
use App\Models\Photo;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class PhotosController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $photos = Photo::all();
        return view('admin.photos.index', compact('photos'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('admin.photos.create');
    }


    /**
     * @param Photo $photo
     * @return Application|Factory|View
     */
    public function show(Photo $photo)
    {
        return view('admin.photos.show', compact('photo'));
    }

    /**
     * @param PhotoRequest $request
     * @return RedirectResponse
     */
    public function store(PhotoRequest $request)
    {
        $photo = PhotoForm::execute($request);
        return redirect()->route('admin.photos.index')->with('status', "{$photo->title} successfully created!");
    }

    /**
     * @param Photo $photo
     * @return Application|Factory|View
     */
    public function edit(Photo $photo)
    {
        return view('admin.photos.edit', compact('photo'));
    }

    /**
     * @param PhotoRequest $request
     * @param Photo $photo
     * @return RedirectResponse
     */
    public function update(PhotoRequest $request, Photo $photo)
    {
        $data = $request->all();
        if ($request->hasFile('picture')) {
            $file = $request->file('picture');
            $path = $file->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $photo->update($data);
        return redirect()->action([self::class, 'index'], ['photo' => $photo])->with('status', "Photo {$photo->title} successfully updated!");
    }

    /**
     * @param Photo $photo
     * @return RedirectResponse
     */
    public function destroy(Photo $photo)
    {
        $photo->delete();
        return redirect()->route('admin.photos.index')->with('status', "Photo successfully deleted!");
    }
}
