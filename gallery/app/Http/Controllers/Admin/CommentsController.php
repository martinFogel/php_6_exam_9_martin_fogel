<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Photo;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $comments = Comment::all();
        return view('admin.comments.index', compact('comments'));
    }

    /**
     * @param Request $request
     * @param Photo $photo
     * @return RedirectResponse
     */
    public function store(Request $request, Photo $photo)
    {
        $request->validate([
            'body' => 'required|min:5',
            'rating' => 'required|in:1,2,3,4,5',
            'photo_id' => 'required'
        ]);
        $comment = new Comment();
        $comment->user_id = $request->user()->id;
        $comment->body = $request->input('body');
        $comment->rating = $request->input('rating');
        $comment->photo_id = $photo->id;
        $comment->save();
        return back();
    }

    /**
     * @param Comment $comment
     * @return Application|Factory|View
     */
    public function edit(Comment $comment)
    {
        $photos = Photo::all();
        return view('admin.comments.edit', compact('comment', 'photos'));
    }

    /**
     * @param Comment $comment
     * @return Application|Factory|View
     */
    public function create(Comment $comment)
    {
        $photos = Photo::all();
        return view('admin.comments.create', compact('comment', 'photos'));
    }

    /**
     * @param Request $request
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function update(Request $request, Comment $comment)
    {
        $request->validate([
            'body' => 'required|min:5',
            'rating' => 'required|in:1,2,3,4,5'
        ]);
        $comment->update($request->all());
        return redirect()->route('admin.comments.index', $comment->photo_id);
    }

    /**
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        return redirect()->route('admin.comments.index', $comment->photo_id);
    }
}
