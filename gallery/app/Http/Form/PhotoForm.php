<?php

namespace App\Http\Form;

use App\Models\Photo;
use Illuminate\Http\Request;

class PhotoForm extends Form
{
    /**
     * @param Request $request
     * @return mixed
     */
    protected function handle(Request $request)
    {
        $data = $request->all();
        $data['user_id'] = auth()->user()->id;
        $file = $request->file('picture');
        if (!is_null($file)) {
            $path = $file->store('pictures', 'public');
            $data['picture'] = $path;
        }
        return Photo::create($data);
    }
}
