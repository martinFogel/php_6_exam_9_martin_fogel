<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PhotoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => ['required', 'max:150'],
            'picture' => 'image|mimes:jpg,png|required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Это обязательное поле: :attribute',
            'title.max' => 'Нужно ввести данные в поле: :attribute',
            'picture.required' => 'Это обязательное поле: :attribute',
            'picture.image' => 'Неправильный формат фото::attribute',
        ];
    }
}
