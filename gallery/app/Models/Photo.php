<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static Paginate(int $int)
 */
class Photo extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'picture', 'user_id'];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class)->latest();
    }


    /**
     * @return HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class)->latest();
    }
}
