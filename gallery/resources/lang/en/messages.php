<?php
return [
    'editComment' => 'Edit Comment',
    'chooseRating' => 'Choose Rating',
    'more' => 'More',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'body' => 'Body',
    'update' => 'Update',
    'createPhoto' => 'Create photo',
    'title' => 'Title',
    'author' => 'Author',
    'chooseFile' => 'Choose file',
    'comments' => 'Comments',
    'createComment' => 'Create comment',
    'submit' => 'Submit',
    'addPhoto' => 'Add photo',
    'home' => 'Home',
    'myProfile' => 'My profile',
    'photo' => 'Photos',
    'main' => 'Photo Gallery'
];
