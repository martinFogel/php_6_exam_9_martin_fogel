<?php
return [
    'french' => 'French',
    'greece'  => 'Greek',
    'russian' => 'Russian',
    'german' => 'German',
    'english' => 'English'
];
