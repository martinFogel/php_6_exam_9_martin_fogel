<?php
return [
    'french' => 'Французкий',
    'greece'  => 'Греческий',
    'russian' => 'Русский',
    'german' => 'Немецкий',
    'english' => 'Английский'
];
