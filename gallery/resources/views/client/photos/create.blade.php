@extends('layouts.app')

@section('content')
    <h1>@lang('messages.createPhoto')</h1>
    <form enctype="multipart/form-data" method="post" action="{{ route('photos.store') }}">
        @csrf
        <div class="form-group">
            <label for="title">@lang('messages.title')</label>
            <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title"
                   value="name">
            @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <div class="custom-file">
                <input type="file" class="custom-file-input @error('picture') is-invalid @enderror" id="customFile"
                       name="picture" value="picture">
                @error('picture')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <label class="custom-file-label" for="customFile">@lang('messages.chooseFile')</label>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">@lang('messages.submit')</button>
    </form>
@endsection
