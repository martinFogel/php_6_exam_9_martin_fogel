@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @foreach($photos as $photo)
                <div class="card m-3" style="width: 18rem;">
                    <img src="{{asset('/storage/' . $photo->picture)}}" class="card-img-top"
                         style="height: 290px; width: auto" alt="{{asset('/storage/' . $photo->picture)}}"
                         width="50px" height="50px">
                    <div class="card-body">
                        <h5 class="card-title"><strong>@lang('messages.title')</strong> {{$photo->title}}</h5>
                        <h5 class="card-title"><strong>@lang('messages.author')</strong> {{$photo->user->name}}</h5>
                        <a href="{{route('photos.show', ['photo' => $photo])}}" class="btn btn-primary">@lang('messages.more')</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="col-md-auto">
            {{$photos->links('pagination::bootstrap-4')}}
        </div>
    </div>
@endsection
