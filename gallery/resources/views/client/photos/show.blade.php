@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="border-bottom pb-3 mb-3">
            <h3 class="pt-5 border-bottom pb-1 mb-3 ">@lang('messages.title') {{$photo->title}}</h3>
            <div class="row row-cols-1 row-cols-md-2 row-cols-lg-2 .img-fluid. w-100%  h-auto ">
                <div class="col mb-4">
                    <h4>@lang('messages.author'):
                        <p>{{$photo->user->name}}</p>
                    </h4>
                    <br>
                </div>
                <div class="col mb-1 ">
                    <img src="{{asset('/storage/' . $photo->picture)}}"
                         class="rounded mx-auto d-block img-fluid rounded float-right"
                         alt="{{asset('/storage/' . $photo->picture)}}" width="200px" height="200px">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3>@lang('messages.comments')</h3>
            </div>
        </div>
        <h1>@lang('messages.createComment')</h1>
        <form method="get" action="{{ route('comments.store', ['photo' => $photo]) }}">
            @csrf
                <label for="rating"><strong>@lang('messages.chooseRating'):</strong></label>
                <select name="rating" id="rating">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            <div class="form-group">
                <label for="description">@lang('messages.body')</label>
                <textarea class="form-control @error('body') is-invalid @enderror" id="body"
                          name="body"></textarea>
                @error('body')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">@lang('messages.submit')</button>
            </form>
        <div class="row">
            <div class="col-8 scrollit">
                @foreach($photo->comments as $comment)
                    @include('client.comments.comment', ['comment' => $comment])
                @endforeach
            </div>
        </div>
@endsection
