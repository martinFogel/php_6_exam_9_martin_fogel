@extends('layouts.app')

@section('content')
    <h1>@lang('messages.editComment')</h1>
    <form method="post" action="{{ route('comments.update', ['comment' => $comment]) }}">
        @method('put')
        @csrf
        <div class="form-group">
        <label for="rating"><strong>@lang('messages.chooseRating')</strong></label>
        <select name="rating" id="rating">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
        </select>
        </div>
        <div class="form-group">
            <label for="description">@lang('messages.body')</label>
            <textarea class="form-control  @error('body') is-invalid @enderror" id="body"
                      name="body">{{$comment->body}}</textarea>
            @error('body')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">@lang('messages.update')</button>
    </form>
@endsection
