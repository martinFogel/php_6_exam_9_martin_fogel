@extends('layouts.app')

@section('content')
    <h2 class="pt-md-5">@lang('messages.photo'):</h2>
        <a href="{{route('photos.create')}}" type="button" class="btn btn-outline-primary">
            @lang('messages.addPhoto')
        </a>
    <div class="row">
        @foreach($photos as $photo)
            <div class="card m-3" style="width: 18rem;">
                <img src="{{asset('/storage/' . $photo->picture)}}" class="card-img-top"
                     style="height: 290px; width: auto" alt="{{asset('/storage/' . $photo->picture)}}"
                     width="50px" height="50px">
                <div class="card-body">
                    <strong>@lang('messages.author'): {{$photo->user->name}}</strong>
                    @can('delete', $photo)
                        <form method="post" action="{{route('photos.destroy', ['photo' => $photo])}}">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-danger">@lang('messages.delete')</button>
                            @endcan
                        </form>
                        <a href="{{route('photos.show', ['photo' => $photo])}}" class="btn btn-primary">@lang('messages.more')</a>
                </div>
            </div>
        @endforeach
    </div>
@endsection
