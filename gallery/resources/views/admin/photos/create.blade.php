@extends('layouts.admin')

@section('content')
    <h1>Create new book</h1>
    <form enctype="multipart/form-data" method="post" action="{{ route('admin.photos.store') }}">
        @csrf
        <div class="form-group">
            <label for="title">Название</label>
            <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title"
                   value="name"/>
            @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <div class="custom-file">
                <input type="file" class="custom-file-input @error('picture') is-invalid @enderror" id="customFile"
                       name="picture" value="picture">
                @error('picture')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
