@extends('layouts.admin')


@section('content')

    <div style="padding-bottom: 30px;">
        <h1>Все фото</h1>
        <a href="{{route('admin.photos.create')}}" type="button" class="btn btn-outline-primary">
           Добавить фото
        </a>
    </div>

    <table class="table" style="padding-top: 30px">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Наименование</th>
            <th scope="col">Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach($photos as $photo)
            <tr>
                <td>
                    {{$photo->title}}
                </td>
                <td>
                    <a class="btn btn-outline-primary" href="{{route('admin.photos.edit', ['photo' => $photo])}}">
                        Редактировать
                    </a>
                    <form method="post" action="{{route('admin.photos.destroy', ['photo' => $photo])}}">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-outline-danger">Удалить</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
