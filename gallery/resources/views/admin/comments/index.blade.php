@extends('layouts.admin')


@section('content')


    <table class="table" style="padding-top: 30px">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Тело</th>
            <th scope="col">Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach($comments as $comment)
            <tr>
                <td>
                    {{$comment->body}}
                </td>
                <td>
                    <form method="post" action="{{route('admin.comments.destroy', ['comment' => $comment])}}">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-outline-danger">Удалить</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
