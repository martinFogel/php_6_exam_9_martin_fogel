<?php

use App\Http\Controllers\CommentsController;
use App\Http\Controllers\LanguageSwitcherController;
use App\Http\Controllers\PhotosController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController as AdminUsersController;
use App\Http\Controllers\Admin\CommentsController as AdminCommentsController;
use App\Http\Controllers\Admin\PhotosController as AdminPhotosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('language')->group(function () {
    Route::get('/', [PhotosController::class, 'index'])->name('home')->middleware(['language']);
    Route::resource('photos', PhotosController::class)->middleware(['auth', 'language']);
    Auth::routes();
    Route::prefix('admin')->middleware('auth')->name('admin.')->group(function () {
        Route::resources(
            [
                'comments' => AdminCommentsController::class,
                'users' => AdminUsersController::class,
                'photos' => AdminPhotosController::class
            ]
        );
    });
    Route::resource('photos.comments', CommentsController::class)->middleware(['auth', 'language']);
    Route::resource('comments', CommentsController::class)->middleware(['auth', 'language'])->except('store');
    Route::get('/photos/{photo}/comment', [CommentsController::class, 'store'])->name('comments.store');
    Route::resource('users', UserController::class)->middleware(['auth', 'language']);
});

Route::get('language/{locale}', [LanguageSwitcherController::class, 'switcher'])
    ->name('language.switcher')
    ->where('locale', 'en|ru');
